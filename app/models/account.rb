class Account < ApplicationRecord
  TYPES = {bank: 'Bank account', credit: 'Credit card', cash: 'Cash', other: 'Other'}

  belongs_to :currency
  belongs_to :user

  validates :user, presence: true
  validates :currency, presence: true
  validates :account_type, inclusion: {in: TYPES.keys.map(&:to_s),
    message: "%{value} is not valid"}
  validates :current_balance, numericality: true
  validates_numericality_of :credit, greater_than_or_equal_to: 0
end