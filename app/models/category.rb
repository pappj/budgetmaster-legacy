class Category < ApplicationRecord
  belongs_to :user

  validates :user, presence: true
  validates :name, length: {minimum: 3}
end