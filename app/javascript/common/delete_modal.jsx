import Modal from "./modal"

export default class DeleteModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <Modal title={'Deleting ' + this.props.subject} body={'Are you sure you want to delete ' + this.props.subject + '?'}
              hideModal={this.props.hideModal}>
      <button type="button" className="btn btn-danger" onClick={this.props.confirmed}>Yes</button>
      <button type="button" className="btn btn-primary" onClick={this.props.hideModal}>No</button>
    </Modal>
  }

}
