import ReactDOM from 'react-dom'
import 'bootstrap/js/dist/modal'

export default class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount(){
    $(this.modal).modal('show');
    $(this.modal).on('hidden.bs.modal', this.props.hideModal);
  }

  componentWillUnmount(){
    $(this.modal).modal('hide');
  }

  render() {
    return <div id="modal-window" className="modal" ref={modal => this.modal = modal} tabIndex="-1" role="dialog">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{this.props.title}</h5>
            <button type="button" className="close" aria-label="Close" onClick={this.props.hideModal}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <p>{this.props.body}</p>
          </div>
          <div className="modal-footer">
            {this.props.children}
          </div>
        </div>
      </div>
    </div>
  }
}