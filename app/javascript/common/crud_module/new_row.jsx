import FormRow from "./form_row";

export default class NewRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <FormRow {...this.props}>
        <div className="col-1 text-center">
          <button type="submit" className="btn btn-primary">Add</button>
        </div>
      </FormRow>
    );
  }
}