import FormRow from "./form_row";

export default class EditRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <FormRow {...this.props} id={"obj-" + this.props.instance.id}>
        <div className="col-1 text-center">
          <button type="submit" className="btn btn-primary">Save</button>
        </div>
        <div className="col-1">
          <button className="btn btn-primary" onClick={this.props.onCancel}>Cancel</button>
        </div>
      </FormRow>
    );
  }
}