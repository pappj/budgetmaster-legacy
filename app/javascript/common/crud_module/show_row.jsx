export default class ShowRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderField = (field, key) => {
    const instance = this.props.instance;
    let value;
    switch(field.type) {
      case "select":
        value = field.options[instance[field.name]];
        break;
      default:
        value = instance[field.name];
        break;
    }

    return <div className={field.width} key={key}>{String(value)}</div>;
  };

  render() {
    return (
      <div id={"obj-" + this.props.instance.id} className="row">
        {this.props.fields.map((field, i) => this.renderField(field, i))}
        <div className="col-1"><button className="btn btn-primary" onClick={this.props.edit}>Edit</button></div>
        <div className="col-1"><button className="btn btn-danger" onClick={this.props.delete}>Delete</button></div>
      </div>
    );
  }

}
