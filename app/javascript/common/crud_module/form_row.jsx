export default class FormRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderField = (field, key) => {
    const instance = this.props.instance;
    let formField;
    switch(field.type) {
      case "text":
        formField = <div className={field.width} key={key}>
            <label className="sr-only">{field.label}</label>
            <input type="text" name={field.name} className="form-control" value={instance[field.name]}
              onChange={this.props.onChange}/>
          </div>;
        break;
      case "select":
        formField = <div className={field.width} key={key}>
            <label className="sr-only">{field.label}</label>
            <select className="form-control" name={field.name} value={instance[field.name]}
              onChange={this.props.onChange}>
              {Object.keys(field.options).map((typeKey, i) => {
                return <option key={i} value={typeKey}>{field.options[typeKey]}</option>
                })
              }
            </select>
          </div>;
        break;
      case "checkbox":
        formField = <div className={field.width} key={key}>
            <label className="sr-only">{field.label}</label>
            <input type="checkbox" name={field.name} className="form-check"
              checked={instance[field.name]} onChange={this.props.onChange}/>
          </div>;
        break;
    }

    return formField;
  }

  render() {
    return (
      <form action="" onSubmit={this.props.onSubmit} id={this.props.id}>
        <div className="form-row">
          {this.props.fields.map((field, i) => this.renderField(field, i))}
          {this.props.children}
        </div>
      </form>
    );
  }
}
