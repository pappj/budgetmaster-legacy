import ShowRow from "./crud_module/show_row";
import EditRow from "./crud_module/edit_row";
import NewRow from "./crud_module/new_row";
import DeleteModal from "./delete_modal";

export default class CRUDModule extends React.Component {
  constructor(props) {
    super(props);

    let instances = props.instances.map((inst) => {
      inst.editing = false;
      return inst;
    });

    this.defaultInstance = {};
    this.props.fields.forEach(field => {
      this.defaultInstance[field.name] = field.default;
    });

    this.state = {
      instances: instances,
      newInstance: Object.assign({}, this.defaultInstance),
      // It is used to store the original account values in case of editing. Properties are the
      // indexes of the accounts.
      originalInstances: {},
      errors: [],
      showDeleteModal: false,
      instanceToDelete: undefined
    };
  }

  editInstance = (evt, index) => {
    evt.preventDefault();

    let originalInstances = Object.assign({}, this.state.originalInstances);
    let instances = this.state.instances.slice();

    originalInstances[index] = Object.assign({}, instances[index]);
    instances[index].editing = true;

    this.setState({
      instances: instances,
      originalInstances: originalInstances
    });
  };

  cancelEditing = (evt, index) => {
    evt.preventDefault();

    let originalInstances = Object.assign({}, this.state.originalInstances);
    let instances = this.state.instances.slice();

    instances[index] = Object.assign({}, originalInstances[index]);
    instances[index].editing = false;
    delete originalInstances[index];

    this.setState({
      instances: instances,
      originalInstances: originalInstances
    });
  };

  updateNewInstance = (evt) => {
    let inst = this.state.newInstance;
    const value = evt.target.type == "checkbox" ? evt.target.checked : evt.target.value;
    inst[evt.target.name] = value;

    this.setState({newInstance: inst});
  };

  updateInstance = (evt, i) => {
    let inst = Object.assign({}, this.state.instances[i]);
    const value = evt.target.type == "checkbox" ? evt.target.checked : evt.target.value;
    inst[evt.target.name] = value;

    let newInstances = this.state.instances.slice();
    newInstances[i] = inst;

    this.setState({instances: newInstances});
  };

  createInstance = (evt) => {
    evt.preventDefault();
    let inst = this.state.newInstance;

    const data = {
      [this.props.modelName]: inst,
      authenticity_token: this.props.token
    };

    $.ajax({
      method: "post",
      url: this.props.submitUrl,
      data: data,
      dataType: "json",
      success: (response) => {
        let newInst = response;
        newInst.editing = false;
        this.setState({
          instances: [...this.state.instances, newInst],
          newInstance: Object.assign({}, this.defaultInstance)
        });
      },
      error: (response) => {
        const err = JSON.parse(response.responseText);
        this.setState(err);
      }
    });
  };

  patchInstance = (evt, index) => {
    evt.preventDefault();
    let instances = this.state.instances.slice();
    const inst = instances[index];

    const data = {
      [this.props.modelName]: inst,
      authenticity_token: this.props.token
    };

    $.ajax({
      method: "patch",
      url: this.props.submitUrl,
      data: data,
      dataType: "json",
      success: (response) => {
        inst.editing = false;
        let originalInstances = Object.assign({}, this.state.originalInstances);
        delete originalInstances[index];

        this.setState({
          instances: instances,
          originalInstances: originalInstances
        });
      },
      error: (response) => {
        const err = JSON.parse(response.responseText);
        this.setState(err);
      }
    });
  };

  showDeleteModal = (evt, i) => {
    evt.preventDefault();
    this.setState({
      showDeleteModal: true,
      instanceToDelete: i
    })
  }

  hideDeleteModal = (evt) => {
    evt.preventDefault();
    this.setState({
      showDeleteModal: false,
      instanceToDelete: undefined
    })
  }

  deleteInstance = (evt) => {
    evt.preventDefault();
    this.hideDeleteModal(evt);
    const index = this.state.instanceToDelete;

    const inst = this.state.instances[index];

    const data = {
      [this.props.modelName]: inst,
      authenticity_token: this.props.token
    };

    $.ajax({
      method: "delete",
      url: this.props.submitUrl,
      data: data,
      dataType: "json",
      success: (response) => {
        let instances = this.state.instances.slice();
        instances.splice(index, 1);

        this.setState({
          instances: instances
        });
      },
      error: (response) => {
        const err = JSON.parse(response.responseText);
        this.setState(err);
      }
    });
  }


  render() {
    let deleteModal = "";
    if(this.state.showDeleteModal) {
      const instanceName = this.state.instances[this.state.instanceToDelete].name;
      deleteModal = <DeleteModal ref={dm => this.deleteModal = dm} subject={instanceName}
                       hideModal={(evt) => this.hideDeleteModal(evt)} confirmed={(evt) => this.deleteInstance(evt)}/>
    }

    return <div>
      <h1>{this.props.title}</h1>
      {this.state.errors.map((err, i) => {
        return <p key={i} className="alert alert-danger">{err}</p>;
      })}
      <div id={this.props.listId}>
        <div className="row">
          {this.props.fields.map((field, i) => {
            return <div key={i} className={field.width}>{field.label}</div>
          })}
        </div>
        {this.state.instances.map((inst, i) => {
            if(inst.editing) {
              return <EditRow instance={inst} fields={this.props.fields} key={i}
                      onChange={(evt) => this.updateInstance(evt, i)} onCancel={(evt) => this.cancelEditing(evt, i)}
                      onSubmit={(evt) => this.patchInstance(evt, i)}/>
            } else {
              return <ShowRow instance={inst} fields={this.props.fields}  key={i}
                      edit={(evt) => this.editInstance(evt, i)} delete={(evt) => this.showDeleteModal(evt, i)}/>
            }
          })
        }
      </div>
      <h3>New {this.props.modelName}</h3>
      <NewRow instance={this.state.newInstance} fields={this.props.fields} onChange={(evt) => this.updateNewInstance(evt)}
        onSubmit={(evt) => this.createInstance(evt)} id={"new-" + this.props.modelName}/>
      {deleteModal}
    </div>
  }
}