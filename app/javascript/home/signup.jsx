export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      password2: ""
    }
  }

  changeEmail = (event) => {
    event.preventDefault();
    this.setState({email: event.target.value});
  };

  changePassword = (event) => {
    event.preventDefault();
    const oldPwd = {password: this.state.password, password2: this.state.password2};
    let newState = {[event.target.name]: event.target.value};
    const newPwd = Object.assign({}, oldPwd, newState);
    if(newPwd.password !== newPwd.password2) {
      this.props.updateErrors(["The passwords don't match"]);
    } else {
      // Clearing the previous error if there was any.
      this.props.updateErrors([]);
    }

    this.setState(newState);
  };


  handleSubmit = (event) => {
    event.preventDefault();
    const errors = this.validateForm();

    if(Object.keys(errors).length === 0) {
      const data = {
        user: {
          email: this.state.email,
          password: this.state.password,
          password_confirmation: this.state.password2
        },
        authenticity_token: this.props.token
      };

      $.ajax({
        method: "post",
        url: "/users",
        data: data,
        dataType: "json",
        success: (response) => {
          window.location = "/accounts";
        },
        error: (response) => {
          const err = JSON.parse(response.responseText);
          console.log(err);
          console.log(Object.values(err));
          this.props.updateErrors(Object.values(err.errors));
        }
      });
    } else {
      this.props.updateErrors(errors);
    }
  };

  render() {
    return (
      <form id="sign-up" action="" onSubmit={this.handleSubmit} className="py-md-3">
        <input type="hidden" name="authenticity_token" value={this.props.token}/>
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="signup-email">Email</label>
            <input id="signup-email" type="email" name="email" className="form-control"
              value={this.state.email} onChange={this.changeEmail} placeholder="your@email.com"/>
          </div>
        </div>
        <div className="form row">
          <div className="form-group col-md-6">
            <label htmlFor="signup-password">Password</label>
            <input id="signup-password" type="password" name="password" className="form-control"
              value={this.state.password} onChange={this.changePassword}/>
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="signup-password2">Password again</label>
            <input id="signup-password2" type="password" name="password2" className="form-control"
              value={this.state.password2} onChange={this.changePassword}/>
          </div>
        </div>
        <button type="submit" className="btn btn-primary">{this.props.submit}</button>
      </form>
    );
  }

  validEmail(email) {
    const pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+[^<>()\.,;:\s@\"]{2,})$/;
    return pattern.test(email);
  }

  validateForm = () => {
    let errors = [];
    if(!this.validEmail(this.state.email)) {
      errors.push("Please enter a valid email address");
    }
    if(this.state.password !== this.state.password2) {
      errors.push("The passwords don't match");
    }

    return errors;
  }
}
