export default class Signin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    }
  }

  handleChange = (event) => {
    event.preventDefault();
    this.setState({[event.target.name]: event.target.value});
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const data = {
      user: {
        email: this.state.email,
        password: this.state.password
      },
      authenticity_token: this.props.token
    };

    $.ajax({
      method: "post",
      url: "/users/sign_in",
      data: data,
      dataType: "json",
      success: (response) => {
        window.location = "/accounts";
      },
      error: (response) => {
        const err = JSON.parse(response.responseText);
        this.props.updateErrors([err.error]);
        this.setState({password: ""});
      }
    })
  };

  render() {
    return (
      <form id="sign-in" action="" onSubmit={this.handleSubmit} className="py-md-3">
        <input type="hidden" name="authenticity_token" value={this.props.token}/>
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="signin-email">Email</label>
            <input id="signin-email" type="email" name="email" className="form-control"
              value={this.state.email} onChange={this.handleChange} placeholder="your@email.com"/>
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="signin-password">Password</label>
            <input id="signin-password" type="password" name="password" className="form-control"
              value={this.state.password} onChange={this.handleChange}/>
          </div>
        </div>
        <button type="submit" className="btn btn-primary">{this.props.submit}</button>
      </form>
    );
  }
}
