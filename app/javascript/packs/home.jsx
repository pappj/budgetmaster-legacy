//import React from 'react';
import ReactDOM from 'react-dom'
import Signup from "../home/signup";
import Signin from "../home/signin";

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: []
    };
  }

  updateErrors = (err) => {
    this.setState({
      errors: err
    })
  };

  render() {
    return <div>
      <h1>Budget Master</h1>
      {this.state.errors.map((err, i) => {
        return <p key={i} className="alert alert-danger">{err}</p>;
      })}
      <h2>Sign in</h2>
      <Signin submit={this.props.data.signin} updateErrors={(err) => this.updateErrors(err)} token={this.props.data.token}/>
      <h2>Sign up</h2>
      <Signup submit={this.props.data.signup} updateErrors={(err) => this.updateErrors(err)} token={this.props.data.token}/>
    </div>
  };
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('home_data');
  const data = JSON.parse(node.getAttribute('data'));
  ReactDOM.render(
    <Home data={data} />,
    document.getElementById('react-main'),
  )
});
