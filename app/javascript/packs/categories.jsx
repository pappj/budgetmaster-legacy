import ReactDOM from 'react-dom'
import CRUDModule from '../common/crud_module'

export default class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.categoryProps = {
      modelName: "category",
      title: "Categories",
      listId: "categories",
      submitUrl: "/categories",
      instances: props.data.categories,
      fields: [
        {
          name: "name",
          label: "Name",
          width: "col-7",
          type: "text",
          default: ""
        }
      ]
    }
  }

  render() {
    return <CRUDModule {...this.categoryProps} token={this.props.data.token} />
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('categories_data');
  const data = JSON.parse(node.getAttribute('data'));
  ReactDOM.render(
    <Categories data={data} />,
    document.getElementById('react-main'),
  )
});
