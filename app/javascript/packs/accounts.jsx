import ReactDOM from 'react-dom'
import CRUDModule from '../common/crud_module'

export default class Accounts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.accountProps = {
      modelName: "account",
      title: "Accounts",
      listId: "accounts",
      submitUrl: "/accounts",
      instances: props.data.accounts,
      fields: [
        {
          name: "name",
          label: "Name",
          width: "col-2",
          type: "text",
          default: ""
        },
        {
          name: "account_type",
          label: "Type",
          width: "col-2",
          type: "select",
          options: props.data.types,
          default: Object.keys(props.data.types)[0]
        },
        {
          name: "currency_id",
          label: "Currency",
          width: "col-1",
          type: "select",
          options: props.data.currencies,
          default: Object.keys(props.data.currencies)[0]
        },
        {
          name: "current_balance",
          label: "Amount",
          width: "col-2",
          type: "text",
          default: "0"
        },
        {
          name: "credit",
          label: "Credit limit",
          width: "col-2",
          type: "text",
          default: "0"
        },
        {
          name: "in_plan",
          label: "Included in plan",
          width: "col-1",
          type: "checkbox",
          default: false
        }
      ]
    }
  }

  render() {
    return <CRUDModule {...this.accountProps} token={this.props.data.token} />
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('accounts_data');
  const data = JSON.parse(node.getAttribute('data'));
  ReactDOM.render(
    <Accounts data={data} />,
    document.getElementById('react-main'),
  )
});
