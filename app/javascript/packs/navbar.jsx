import React from 'react';
import ReactDOM from 'react-dom'

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let signOut = "";
    if(this.props.data.userSignedIn) {
      signOut =
        <a className="nav-link navbar-nav" href={this.props.data.signOutUrl}>Sign out</a>
    }

    return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark" id="navbar">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <a className="nav-link" href="/">Home</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/summary">Budget summary</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/accounts">Manage accounts</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/categories">Manage categories</a>
          </li>
        </ul>
        {signOut}
      </div>
    </nav>
  );}
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('users_data');
  const data = JSON.parse(node.getAttribute('data'));
  ReactDOM.render(
    <NavBar data={data}/>,
    document.getElementById('react-navbar'),
  )
});
