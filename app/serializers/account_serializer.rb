class AccountSerializer < ApplicationSerializer
  attributes :id, :name, :account_type, :currency_id, :current_balance, :credit, :in_plan
end
