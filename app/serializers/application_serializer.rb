class ApplicationSerializer < ActiveModel::Serializer
  def id
    ApplicationController::encode_id(object.id)
  end
end
