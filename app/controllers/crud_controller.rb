class CrudController < ApplicationController
  before_action :verify_user

  def create
    inst = create_instance
    success = inst.save

    respond_to do |format|
      format.json {
        if success
          render(json: inst)
        else
          render(json: {errors: inst.errors.full_messages}, status: :unprocessable_entity)
        end
      }
    end
  end

  def update
    inst = get_instance(params[param_key][:id])
    success = inst.update(model_params)

    respond_to do |format|
      format.json {
        if success
          render(json: {})
        else
          render(json: {errors: inst.errors.full_messages}, status: :unprocessable_entity)
        end
      }
    end
  rescue Pundit::NotAuthorizedError
    # Don't let the client know if the object exists or not
    render(json: {errors: ["#{model_name} is not found"]}, status: :not_found)
  end

  def destroy
    inst = get_instance(params[param_key][:id])
    success = inst.destroy

    respond_to do |format|
      format.json {
        if success
          render(json: {})
        else
          render(json: {errors: inst.errors.full_messages}, status: :service_unavailable)
        end
      }
    end
  rescue Pundit::NotAuthorizedError
    # Don't let the client know if the object exists or not
    render(json: {errors: ["#{model_name} is not found"]}, status: :not_found)
  end

  private
    def param_key
      self.controller_name.chomp("Controller").singularize.underscore.to_sym
    end

    def model_name
      self.controller_name.singularize.capitalize
    end

    def verify_user
      unless user_signed_in?
        redirect_to(root_path)
      end
    end
end