class AccountsController < CrudController
  # before_action :verify_user

  def index
    accounts = Account.where(user: current_user).order(:id)
    currencies = Currency.all.map{|ccy| [ccy.id, ccy.short_code]}.to_h
    render("accounts/index",
           locals:{accounts: accounts.map{|acc| ActiveModelSerializers::SerializableResource.new(acc)},
                   currencies: currencies, types: Account::TYPES})
  end

  def create_instance
    acc = Account.new(model_params)
    acc.user = current_user

    return acc
  end

  private
    def model_params
      params.require(:account).permit(:name, :account_type, :currency_id, :current_balance,
        :credit, :in_plan)
    end
end
