class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound do |e|
    render(json: {errors: ["#{e.model} not found"]}, status: :not_found)
  end

  def self.encode_id(id)
    hashids = Hashids.new(Rails.application.secrets.hashids_salt, 8)
    hashids.encode(id)
  end

  def self.decode_id(id)
    hashids = Hashids.new(Rails.application.secrets.hashids_salt, 8)
    hashids.decode(id)[0]
  end

  def get_instance(id)
    authorize controller_name.classify.constantize.find(ApplicationController::decode_id(id))
  end
end
