class CategoriesController < CrudController
  def index
    categories = Category.where(user: current_user).order(:id)
    render("categories/index",
           locals:{categories: categories.map{|cat| ActiveModelSerializers::SerializableResource.new(cat)}})
  end

  def create_instance
    cat = Category.new(model_params)
    cat.user = current_user
    return cat
  end

  private
    def model_params
      params.require(:category).permit(:name)
    end
end
