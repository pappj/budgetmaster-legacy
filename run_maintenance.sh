#!/bin/sh

file_name="./scripts/docker-compose/server.yml"
app_name="app"

if [ $1 == "test" ]
  then
    file_name="./scripts/docker-compose/test.yml"
    app_name="test"
fi

docker-compose -f $file_name run $app_name bash