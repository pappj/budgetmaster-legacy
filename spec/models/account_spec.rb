require 'rails_helper'

RSpec.describe Account, type: :model do
  subject {
    u = User.new(email: "joe@sample.com", password: "wontfindout")
    c = Currency.new(short_code: "HUF", symbol: "Ft")
    Account.new(account_type: "bank", current_balance: 1000, credit: 500, user: u, currency: c) }
  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid with invalid type" do
      subject.account_type = "gimmethecash"
      expect(subject).to_not be_valid
    end

    it "is not valid with non-number balance" do
      subject.current_balance = "thousand"
      expect(subject).to_not be_valid
    end

    it "is not valid with non-number credit" do
      subject.credit = "hundred"
      expect(subject).to_not be_valid
    end

    it "is not valid with negative credit" do
      subject.credit = -500
      expect(subject).to_not be_valid
    end
  end
end
