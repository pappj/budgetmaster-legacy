require 'rails_helper'

RSpec.describe "New account", js: true do
  include_context "user login"

  before(:each) do
    login_user
    visit accounts_path
  end

  it "adds new account on success" do
    within("#new-account") do
      fill_in('name', with: 'Erste account')
      select('Cash', from: 'account_type')
      select('EUR', from: 'currency_id')
      fill_in('current_balance', with: 1680)
      check('in_plan')
      click_button('Add')
    end

    section = find("#accounts")
    expect(section).to have_content("Erste account")
    expect(section).to have_content("EUR")
    expect(section).to have_content("1680")
  end

  it "creates the database entry on success" do
    within("#new-account") do
      fill_in('name', with: 'Erste account')
      select('Cash', from: 'account_type')
      select('EUR', from: 'currency_id')
      fill_in('current_balance', with: 1680)
      check('in_plan')
      click_button('Add')
    end

    sleep(1)

    account = Account.find_by(name: "Erste account")
    currency = Currency.find_by(short_code: "EUR")

    expect(account).not_to be(nil)
    expect(account.user).to eq(user)
    expect(account.currency).to eq(currency)
    expect(account.account_type).to eq("cash")
  end

  it "clears new account form on success" do
    within("#new-account") do
      fill_in('name', with: 'Erste account')
      select('Cash', from: 'account_type')
      select('EUR', from: 'currency_id')
      fill_in('current_balance', with: 1680)
      check('in_plan')
      click_button('Add')
    end

    expect(find("#new-account input[name='name']").value).to eq("")
    expect(find("#new-account select[name='account_type']").value).to eq("bank")
    expect(find("#new-account input[name='current_balance']").value).to eq("0")
    expect(find("#new-account input[name='credit']").value).to eq("0")
    expect(find("#new-account input[name='in_plan']")).not_to be_checked
  end

  it "shows error on negative credit" do
    within("#new-account") do
      fill_in('name', with: 'Erste account')
      select('Credit card', from: 'account_type')
      select('EUR', from: 'currency_id')
      fill_in('current_balance', with: 1680)
      fill_in('credit', with: -500)
      check('in_plan')
      click_button('Add')
    end

    expect(page).to have_content("Credit must be greater than or equal to 0")
    expect(find("#new-account input[name='name']").value).to eq("Erste account")
    expect(find("#new-account select[name='account_type']").value).to eq("credit")
    expect(find("#new-account input[name='current_balance']").value).to eq("1680")
    expect(find("#new-account input[name='credit']").value).to eq("-500")
    expect(find("#new-account input[name='in_plan']")).to be_checked
  end
end