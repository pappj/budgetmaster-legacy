require 'rails_helper'

RSpec.describe "Delete account", js: true do
  include_context "user login"

  before(:each) do
    currency = Currency.find_by(short_code: "HUF")
    Account.create(name: "Sample account", account_type: "bank", currency: currency,
                   current_balance: 146300, credit: 0, in_plan: false, user: user)
    login_user
    visit accounts_path
  end

  after(:each) do
    account = Account.find_by(name: "Sample account")
    account&.destroy
  end

  it "removes the account from the page" do
    expect(page).to have_content("Sample account")

    within("#accounts") do
      click_button('Delete')
    end

    within("#modal-window") do
      click_button('Yes')
    end

    sleep(1)

    expect(page).not_to have_content("Sample account")
  end

  it "removes the account from the database" do
    expect(page).to have_content("Sample account")

    within("#accounts") do
      click_button('Delete')
    end

    within("#modal-window") do
      click_button('Yes')
    end

    sleep(1)

    account = Account.find_by(name: "Sample account")
    expect(account).to be(nil)
  end

  it "doesn't remove the account on cancel" do
    expect(page).to have_content("Sample account")

    within("#accounts") do
      click_button('Delete')
    end

    within("#modal-window") do
      click_button('No')
    end

    expect(page).to have_content("Sample account")
    account = Account.find_by(name: "Sample account")
    expect(account).not_to be(nil)
  end
end