require 'rails_helper'

RSpec.describe "Edit account", js: true do
  include_context "user login"

  before(:each) do
    currency = Currency.find_by(short_code: "HUF")
    Account.create(name: "Sample account", account_type: "bank", currency: currency,
                   current_balance: 146300, credit: 0, in_plan: false, user: user)
    login_user
    visit accounts_path
  end

  after(:each) do
    account = Account.find_by(name: "Sample account")
    account&.destroy
  end

  def get_id
    acc = Account.find_by(name: "Sample account")
    id = ApplicationController::encode_id(acc.id)
    return "#obj-#{id}"
  end

  it "edits the account successfully" do
    expect(page).to have_content("Sample account")
    within("#accounts") do
      click_button("Edit")
    end

    within(get_id) do
      fill_in('name', with: 'Another name')
      select('Cash', from: 'account_type')
      select('EUR', from: 'currency_id')
      fill_in('current_balance', with: 1680)
      click_button('Save')
    end

    section = find("#accounts")
    expect(section).to have_content("Another name")
    expect(section).to have_content("EUR")
    expect(section).to have_content("1680")
    expect(section).not_to have_content("Sample account")
    expect(section).not_to have_content("HUF")
    expect(section).not_to have_content("146300")
  end

  it "resets the account on cancel" do
    expect(page).to have_content("Sample account")
    within("#accounts") do
      click_button("Edit")
    end

    within(get_id) do
      fill_in('name', with: 'Another name')
      select('Cash', from: 'account_type')
      select('EUR', from: 'currency_id')
      fill_in('current_balance', with: 1680)
      click_button('Cancel')
    end

    section = find("#accounts")
    expect(section).not_to have_content("Another name")
    expect(section).not_to have_content("EUR")
    expect(section).not_to have_content("1680")
    expect(section).to have_content("Sample account")
    expect(section).to have_content("HUF")
    expect(section).to have_content("146300")
  end
end