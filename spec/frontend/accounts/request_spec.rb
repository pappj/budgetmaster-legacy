require 'rails_helper'

RSpec.describe "Accounts request forging", type: :request do
  include_context "user login"

  before(:each) do
    post '/users/sign_in', xhr: true, as: :json, params: {user: {email: "john@doe.com", password: "verysecured"}}
    currency = Currency.find_by(short_code: "HUF")
    acc = Account.create(name: "Temp acc", account_type: "other", currency: currency,
                        current_balance: 7000, credit: 0, in_plan: false, user: other_user)
  end

  def get_id
    acc = Account.find_by(name: "Temp acc")
    id = ApplicationController::encode_id(acc.id)
  end

  it "can't edit others' accounts" do
    patch '/accounts', xhr: true, as: :json, params: {account: {id: get_id, current_balance: 12320, credit: 200}}

    expect(response).to have_http_status(:not_found)
    acc2 = Account.find_by(name: "Temp acc")
    expect(acc2.current_balance).to eq(7000)
    expect(acc2.credit).to eq(0)
    acc2.destroy
  end

  it "can't delete others' accounts" do
    delete '/accounts', xhr: true, as: :json, params: {account: {id: get_id}}

    expect(response).to have_http_status(:not_found)
    acc2 = Account.find_by(name: "Temp acc")
    expect(acc2).not_to be(nil)
    acc2.destroy
  end
end