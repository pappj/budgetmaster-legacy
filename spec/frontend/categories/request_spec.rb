require 'rails_helper'

RSpec.describe "Categories request forging", type: :request do
  include_context "user login"

  before(:each) do
    post '/users/sign_in', xhr: true, as: :json, params: {user: {email: "john@doe.com", password: "verysecured"}}
    Category.create(name: "Temp cat", user: other_user)
  end

  def get_id
    cat = Category.find_by(name: "Temp cat")
    id = ApplicationController::encode_id(cat.id)
  end

  it "can't edit others' categories" do
    patch '/categories', xhr: true, as: :json, params: {category: {id: get_id, name: "Fancy watches"}}

    expect(response).to have_http_status(:not_found)
    cat = Category.find_by(name: "Fancy watches")
    expect(cat).to eq(nil)
    cat2 = Category.find_by(name: "Temp cat")
    cat2.destroy
  end

  it "can't delete others' categories" do
    delete '/categories', xhr: true, as: :json, params: {category: {id: get_id}}

    expect(response).to have_http_status(:not_found)
    cat2 = Category.find_by(name: "Temp cat")
    expect(cat2).not_to be(nil)
    cat2.destroy
  end
end