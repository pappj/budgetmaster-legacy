require 'rails_helper'

RSpec.describe "Edit category", js: true do
  include_context "user login"

  before(:each) do
    Category.create(name: "Car maintenance", user: user)
    login_user
    visit categories_path
  end

  after(:each) do
    category = Category.find_by(name: "Car maintenance")
    category&.destroy
  end

  def get_id
    cat = Category.find_by(name: "Car maintenance")
    id = ApplicationController::encode_id(cat.id)
    return "#obj-#{id}"
  end

  it "edits the category successfully" do
    expect(page).to have_content("Car maintenance")
    within("#categories") do
      click_button("Edit")
    end

    within(get_id) do
      fill_in('name', with: 'Another name')
      click_button('Save')
    end

    section = find("#categories")
    expect(section).to have_content("Another name")
    expect(section).not_to have_content("Car maintenance")
  end

  it "resets the category on cancel" do
    expect(page).to have_content("Car maintenance")
    within("#categories") do
      click_button("Edit")
    end

    within(get_id) do
      fill_in('name', with: 'Another name')
      click_button('Cancel')
    end

    section = find("#categories")
    expect(section).not_to have_content("Another name")
    expect(section).to have_content("Car maintenance")
  end
end