require 'rails_helper'

RSpec.describe "Delete category", js: true do
  include_context "user login"

  before(:each) do
    Category.create(name: "Fast food", user: user)
    login_user
    visit categories_path
  end

  after(:each) do
    category = Category.find_by(name: "Fast food")
    category&.destroy
  end

  it "removes the category from the page" do
    expect(page).to have_content("Fast food")

    within("#categories") do
      click_button('Delete')
    end

    within("#modal-window") do
      click_button('Yes')
    end

    sleep(1)

    expect(page).not_to have_content("Fast food")
  end

  it "removes the category from the database" do
    expect(page).to have_content("Fast food")

    within("#categories") do
      click_button('Delete')
    end

    within("#modal-window") do
      click_button('Yes')
    end

    sleep(1)

    category = Category.find_by(name: "Fast food")
    expect(category).to be(nil)
  end

  it "doesn't remove the category on cancel" do
    expect(page).to have_content("Fast food")

    within("#categories") do
      click_button('Delete')
    end

    within("#modal-window") do
      click_button('No')
    end

    expect(page).to have_content("Fast food")
    category = Category.find_by(name: "Fast food")
    expect(category).not_to be(nil)
  end
end