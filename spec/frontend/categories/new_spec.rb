require 'rails_helper'

RSpec.describe "New category", js: true do
  include_context "user login"

  before(:each) do
    login_user
    visit categories_path
  end

  it "adds new category on success" do
    within("#new-category") do
      fill_in('name', with: 'Clothes')
      click_button('Add')
    end

    section = find("#categories")
    expect(section).to have_content("Clothes")
  end

  it "creates the database entry on success" do
    within("#new-category") do
      fill_in('name', with: 'Clothes')
      click_button('Add')
    end

    sleep(1)

    category = Category.find_by(name: "Clothes")

    expect(category).not_to be(nil)
    expect(category.user).to eq(user)
  end

  it "clears new category form on success" do
    within("#new-category") do
      fill_in('name', with: 'Clothes')
      click_button('Add')
    end

    expect(find("#new-category input[name='name']").value).to eq("")
  end

  it "shows error on short name" do
    within("#new-category") do
      fill_in('name', with: 'Ad')
      click_button('Add')
    end

    expect(page).to have_content("Name is too short (minimum is 3 characters)")
    expect(find("#new-category input[name='name']").value).to eq("Ad")
  end
end