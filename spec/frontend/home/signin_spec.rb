require 'rails_helper'

RSpec.describe "Sign in", js: true do
  before(:each) do
    visit root_path
  end

  it "shows an error if credentials are wrong" do
    within("#sign-in") do
      fill_in 'Email', with: 'test@user.com'
      fill_in 'Password', with: 'dummy-pwd'
      click_button 'Sign in'
    end

    expect(page).to have_content("Invalid Email or password")
  end

  context "when the user really exists" do
    before do
      user = User.new(email: "good@boy.com", password: "verysecured")
      user.save
    end

    it "signs in successfully" do
      within("#sign-in") do
        fill_in 'Email', with: 'good@boy.com'
        fill_in 'Password', with: 'verysecured'
        click_button 'Sign in'
      end

      expect(page).to have_current_path("/accounts")
    end

    it "signs out successfully" do
      within("#sign-in") do
        fill_in 'Email', with: 'good@boy.com'
        fill_in 'Password', with: 'verysecured'
        click_button 'Sign in'
      end

      within("#navbar") do
        click_link 'Sign out'
      end

      expect(page).to have_current_path("/")
      expect(page).not_to have_content("Sign out")
    end
  end
end