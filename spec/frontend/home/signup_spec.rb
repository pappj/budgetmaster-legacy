require 'rails_helper'

RSpec.describe "Sign up", js: true do
  before(:each) do
    visit root_path
  end

  it "works for acceptable parameters" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@test.com'
      fill_in 'Password', with: 'totallysecure'
      fill_in 'Password again', with: 'totallysecure'
      click_button 'Sign up'
    end

    expect(page).to have_current_path("/accounts")
  end

  it "shows error when passwords don't match" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@test.com'
      fill_in 'Password', with: 'totallysecure'
      fill_in 'Password again', with: 'somethingelse'
      click_button 'Sign up'
    end

    expect(page).to have_content("The passwords don't match")
  end

  it "shows error when password is too short" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@test.com'
      fill_in 'Password', with: 'ok'
      fill_in 'Password again', with: 'ok'
      click_button 'Sign up'
    end

    expect(page).to have_content("too short")
  end

  it "shows error if the email is not correct" do
    within("#sign-up") do
      fill_in 'Email', with: 'user@.com'
      fill_in 'Password', with: 'totallysecure'
      fill_in 'Password again', with: 'totallysecure'
      click_button 'Sign up'
    end

    expect(page).to have_selector("#sign-up")
  end

  context "when the user already exists" do
    before do
      user = User.new(email: "user@test.com", password: "buzzword")
      user.save
    end

    it "shows error for the same email address" do
      within("#sign-up") do
        fill_in 'Email', with: 'user@test.com'
        fill_in 'Password', with: 'totallysecure'
        fill_in 'Password again', with: 'totallysecure'
        click_button 'Sign up'
      end

      expect(page).to have_content("has already been taken")
    end
  end
end
