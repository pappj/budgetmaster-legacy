require 'rails_helper'

RSpec.shared_context "user login" do
  let(:user) {User.find_by(email: "john@doe.com")}
  let(:other_user) {User.find_by(email: "foo@bar.com")}

  before(:all) do
    User.create(email: "john@doe.com", password: "verysecured")
    User.create(email: "foo@bar.com", password: "foobar")
  end

  after(:all) do
    user = User.find_by(email: "john@doe.com")
    user.destroy
    other_user = User.find_by(email: "foo@bar.com")
    other_user.destroy
  end

  def login_user
    login_as(user, scope: :user)
  end
end