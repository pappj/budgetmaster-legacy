Rails.application.routes.draw do
  devise_for :users, controllers: {registrations: "users/registrations", sessions: "users/sessions"}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"

  get "/accounts", to: "accounts#index"#, as: "accounts_path"
  post "/accounts", to: "accounts#create"#, as: "new_accounts_path"
  patch "/accounts", to: "accounts#update"#, as: "edit_accounts_path"
  delete "/accounts", to: "accounts#destroy"#, as: "delete_accounts_path"

  get "/categories", to: "categories#index"
  post "/categories", to: "categories#create"
  patch "/categories", to: "categories#update"
  delete "/categories", to: "categories#destroy"
end
